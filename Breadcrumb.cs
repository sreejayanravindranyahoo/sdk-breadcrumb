﻿using System.Collections.Generic;

namespace SDK.Models.Components.Breadcrumb
{
    public class Breadcrumb
    {
       
        public bool HideBreadcrumb { get; set; }
        public List<MenuItem> BreadCrumb { get; set; }
        public MenuItem CurrentItem { get; set; }
      
    }
       
    
}
