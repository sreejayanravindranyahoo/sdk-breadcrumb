﻿using SDK.Repositories.Interfaces;
using SDK.Repositories.Repositories;
using System.Web.Mvc;

namespace SDK.Web.Controllers
{
    public class BreadcrumbComponentController : Controller
    {
        private readonly IBreadcrumbRepository _breadCrumbService;

        public BreadcrumbComponentController(IBreadcrumbRepository breadcrumbService)
        {
            
           _breadCrumbService = breadcrumbService;
        }

        public ActionResult Index()
        {
          /// var _breadCrumbService = new BreadcrumbRepository();
            var model = _breadCrumbService.GetBreadCrumbData();
            return View(model);
        }
    }
}