﻿using SDK.Models.Components.Breadcrumb;
using SDK.Repositories.Interfaces;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;
using System.Collections.Generic;

namespace SDK.Repositories.Repositories
{
    public class BreadcrumbRepository : IBreadcrumbRepository
    {
            
        public  Breadcrumb GetBreadCrumbData()
        {
            var item = Context.Item;
            var menu = new Breadcrumb();

            if (item.Fields["Hide from breadcrumb"].Value == "1")
            {
                return new Breadcrumb() { HideBreadcrumb = true };
            }
            else
            {
                string[] paths = item.Paths.LongID.Split('/');

                if (paths.Length > 3)
                {
                    var menuItems = new List<MenuItem>();

                    for (int i = 4; i < paths.Length - 1; i++)
                    {
                       
                        var currentItem = Sitecore.Context.Database.GetItem(paths[i]);

                        bool isExcludeFromBreadCrumb = ExitLoop(currentItem);
                        if (isExcludeFromBreadCrumb == true)
                        {
                            return new Breadcrumb() { HideBreadcrumb = true };
                        }

                        else
                        {
                            menuItems.Add(new MenuItem() { Name = currentItem.Name, Url = LinkManager.GetItemUrl(currentItem, SDK.Common.Extensions.SitecoreCommon.GetUrlOptions()) });
                        }
                    }
                
                    menu.CurrentItem = paths.Length==4?null: new MenuItem() { Name = item.Name, Url = LinkManager.GetItemUrl(item, SDK.Common.Extensions.SitecoreCommon.GetUrlOptions()) };

                    menu.BreadCrumb = menuItems;


                }
                return menu;
            }
        }
        private  bool ExitLoop(Item currentItem)
        {
            bool isExcludeFromBreadCrumb = currentItem.Fields["Hide from breadcrumb"].Value != "1" ? false : true;

            return isExcludeFromBreadCrumb;

        }
    }
}
